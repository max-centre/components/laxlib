.SUFFIXES :
.SUFFIXES : .o .c .f .f90

.f90.o:
	$(MPIF90) $(F90FLAGS) -c $<

# .f.o and .c.o: do not modify

.f.o:
	$(F77) $(FFLAGS) -c $<

.c.o:
	$(CC) $(CFLAGS)  -c $<

MANUAL_DFLAGS  =
DFLAGS         =  -D__DFTI -D__MPI
FDFLAGS        = $(DFLAGS) $(MANUAL_DFLAGS)

IFLAGS         = -I/cineca/prod/opt/compilers/intel/pe-xe-2018/binary/mkl/include

MOD_FLAG      = -I

MPIF90         = mpiifort
F90           = ifort
CC             = icc
F77            = ifort

# GPU architecture (Kepler: 35, Pascal: 60, Volta: 70 )
GPU_ARCH=

# CUDA runtime (Pascal: 8.0, Volta: 9.0)
CUDA_RUNTIME=

# CUDA F90 Flags
CUDA_F90FLAGS=

CPP            = cpp
CPPFLAGS       = -P -traditional $(DFLAGS) $(IFLAGS)

CFLAGS         = -O3 $(DFLAGS) $(IFLAGS)
F90FLAGS       = $(FFLAGS) -nomodule -qopenmp -fpp $(FDFLAGS) $(CUDA_F90FLAGS) $(IFLAGS) $(MODFLAGS)
FFLAGS         = -O2 -assume byterecl -g -traceback -qopenmp

LD             = mpiifort
LDFLAGS        =   -qopenmp
LD_LIBS        = 

BLAS_LIBS      =   -lmkl_intel_lp64  -lmkl_intel_thread -lmkl_core
LAPACK_LIBS    = 
SCALAPACK_LIBS = 

MPI_LIBS       = 
MASS_LIBS      = 
CUDA_LIBS=
CUDA_EXTLIBS = 

AR             = ar
ARFLAGS        = ruv

RANLIB         = ranlib

FLIB_TARGETS   = all

