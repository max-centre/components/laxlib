# LAXlib: Unified interface to eigensolvers

## Interface to eigensolvers
The present implementation of LAXlib creates a simple interface to different 
solvers for a generalized real/symmetric or complex/hermitian eigenproblem.

At present four such interface routines are implemeted. Two for the shared memory case:

```
SUBROUTINE lax_diag_real_shared (solver_id,h,s,ld,n,ne,eigenvalues,eigenvectors)
  INTEGER,INTENT(IN)      :: solver_id       !integer to select from the available solvers (see below)
  INTEGER,INTENT(IN)      :: ld              !Leading dimension of matrices h,s,eigenvectors
  INTEGER,INTENT(IN)      :: n               !matrix dimension
  REAL(dp),INTENT(INOUT)  :: h(ld,*),s(ld,*) ! H and S matrices
  INTEGER,INTENT(INOUT)   :: ne              !Number of eigenvalues/vectors
  REAL(dp),INTENT(OUT)    :: eigenvalues(*)  !Eigenvalues (only ne of them)
  REAL(dp),INTENT(OUT)    :: eigenvectors(ld,*) !Eigenvectors 
```

```
SUBROUTINE lax_diag_complex_shared (solver_id,h,s,ld,n,ne,eigenvalues,eigenvectors)
  INTEGER,INTENT(IN)       :: solver_id           !integer to select from the available solvers (see below)
  INTEGER,INTENT(IN)       :: ld                  !Leading dimension of matrices h,s,eigenvectors
  INTEGER,INTENT(IN)       :: n                   !matrix dimension
  COMPLEX(dp),INTENT(INOUT):: h(ld,*),s(ld,*)     ! H and S matrices
  INTEGER,INTENT(INOUT)    :: ne                  !Number of eigenvalues/vectors
  REAL(dp),INTENT(OUT)     :: eigenvalues(*)      !Eigenvalues (only ne of them)
  COMPLEX(dp),INTENT(OUT)  :: eigenvectors(ld,*)  !Eigenvectors 
```

In addition, two further routines are provided that operate on distributed matrices.
In this case the three matrices h,s,eigenvectors have to be provided in a block-cyclic distribution as
used in SCALAPACK together with the corresponding blacs_descriptors.

```
SUBROUTINE lax_diag_real/complex_distributed (solver_id,h,s,ld,ne,eigenvalues,eigenvectors,desc,mpi_comm)
  IMPLICIT NONE
  include "laxlib.fh"
  INTEGER,INTENT(IN)            :: solver_id           ! integer to select from the available solvers (see below)
  REAL/COMPLEX(dp),INTENT(INOUT):: h(ld,*),s(ld,*)     ! H and S matrices
  INTEGER,INTENT(IN)            :: ld                  ! leading dimension of matrices
  INTEGER,INTENT(INOUT)         :: ne                  ! number of eigenvectors requested/obtained
  REAL(dp),INTENT(OUT)          :: eigenvalues(*)      ! Eigenvalues
  REAL/COMPLEX(dp),INTENT(OUT)  :: eigenvectors(ld,*)  ! Eigenvectors
  INTEGER,INTENT(IN)            :: desc(*)             ! blacs descriptor
  INTEGER,INTENT(IN)            :: mpi_comm            ! mpi_communicator
```

## Available solvers

To enable the codes to use the different solvers the following functions are provided. First a simple function providing
the number of implemented solvers in the library. 
```
INTEGER FUNCTION lax_diag_num_solvers()
```
Not all of the solvers might actually be available, this function is used to 
allocate the arrays for the following subroutine:
```
SUBROUTINE lax_diag_solvers(solver_ids,solver_names)
  INTEGER, INTENT(OUT)          :: solver_ids(*)    !use lax_diag_num_solvers to dimension this correctly!
  CHARACTER(len=20),INTENT(OUT) :: solver_names(*)  !use lax_diag_num_solvers to dimension this correctly!
```
By calling this subroutine you obtain a list of implemented solvers with their IDs and names. The following convention for the
IDs is used:
* solver_id<0 means the solver is not available. For example because you did not compile the library with the corresponding solver
* solver_id<100 means this a solver for the shared memory case, i.e. you provide the full matrix.
* solver_id>100 means this solver is expecting you to provide a distributed matrix


## Timing and Error handling

You can attach your custom error-handler and timing routines to the library.
```
subroutine lax_set_timehandler(start,stop)
```
```
subroutine lax_set_errorhandler(error)
```
can be used to set these. The callback routine (start,stop,error) should have the following interface:
```
SUBROUTINE callback(msg)
  CHARACTER(len=*),INTENT(IN)::msg
END SUBROUTINE callback
```     

## Compilation 
TODO