#First check if we can compile with LAPACK
try_compile(LAXLIB_USE_LAPACK ${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/cmake/tests/test_LAPACK.f90)

#Try to add -mkl switch
if (NOT LAXLIB_USE_LAPACK)
      try_compile(LAXLIB_USE_LAPACK ${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/cmake/tests/test_LAPACK.f90
   	    LINK_LIBRARIES "-mkl")
       if (LAXLIB_USE_LAPACK)
           target_link_libraries(lax PUBLIC "-mkl")
       endif()
endif()

if (NOT LAXLIB_USE_LAPACK)
      find_package(LAPACK)
      try_compile(LAXLIB_USE_LAPACK ${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/cmake/tests/test_LAPACK.f90
   	    LINK_LIBRARIES ${LAPACK_LIBRARIES})
       if (LAXLIB_USE_LAPACK)
           target_link_libraries(lax PUBLIC ${LAPACK_LIBRARIES})
       endif()
endif()

message("LAPACK Library found:${LAXLIB_USE_LAPACK}")
