#First check if we can compile with LAPACK
try_compile(LAXLIB_USE_SCALAPACK ${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/cmake/tests/test_SCALAPACK.f90)

#Try typical mkl string
if (NOT LAXLIB_USE_SCALAPACK)
     message("Test for SCALAPCK with mkl flags")
     try_compile(LAXLIB_USE_SCALAPACK ${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/cmake/tests/test_SCALAPACK.f90
           LINK_LIBRARIES "-mkl;-lmkl_scalapack_lp64;-lmkl_blacs_intelmpi_lp64")
     if (LAXLIB_USE_SCALAPACK)
          target_link_libraries(lax PUBLIC "-mkl;-lmkl_scalapack_lp64;-lmkl_blacs_intelmpi_lp64")
     endif()
endif()

message("SCALAPACK Library found:${LAXLIB_USE_SCALAPACK}")
if (LAXLIB_USE_SCALAPACK)
   target_compile_definitions(lax PUBLIC "CPP_SCALAPACK;CPP_MPI")
   #target_compile_definitions(lax PUBLIC "__SCALAPACK;__MPI")
endif()
