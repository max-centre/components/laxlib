MODULE m_error_timing

  ABSTRACT INTERFACE
     SUBROUTINE callback(msg)
       CHARACTER(len=*),INTENT(IN)::msg
     END SUBROUTINE callback
  END INTERFACE

  
  
  PROCEDURE(callback),POINTER :: error=>null()
  PROCEDURE(callback),POINTER :: t_start=>null()
  PROCEDURE(callback),POINTER :: t_stop=>null()

CONTAINS

  SUBROUTINE internal_error(msg)
    CHARACTER(len=*),INTENT(IN)::msg
    PRINT *,msg
    STOP 
    lax_internal_error=0 !never used
  END SUBROUTINE internal_error
  SUBROUTINE do_nothing(msg)
    CHARACTER(len=*),INTENT(IN)::msg
  END SUBROUTINE do_nothing

  SUBROUTINE lax_init()
    IF (.NOT.ASSOCIATED(error)) error=>internal_error
    IF (.NOT.ASSOCIATED(t_start)) t_start=>do_nothing
    IF (.NOT.ASSOCIATED(t_stop)) t_stop=>do_nothing
  END SUBROUTINE lax_init

END MODULE m_error_timing
  

SUBROUTINE lax_set_errorhandler(e)
  USE m_error_timing
  EXTERNAL:: e
  error=>e
END SUBROUTINE lax_set_errorhandler

SUBROUTINE lax_set_timehandler(tstart,tstop)
  USE m_error_timing
  EXTERNAL:: tstart,tstop
  t_start=>tstart
  t_stop=>tstop
END SUBROUTINE lax_set_timehandler
