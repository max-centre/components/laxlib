!IDs for solvers
!id<0 solver not available
!id<100 serial solver
!id>100 parallel solves

#define NUMBER_OF_SOLVERS 7

#define LAPACK_FLEUR 1

#ifdef CPP_MAGMA
#define MAGMA_FLEUR 2
#else
#define MAGMA_FLEUR -2
#endif

#define LAPACK_QE 3

#ifdef CPP_SCALAPACK
#define SCALAPACK_FLEUR 101
#else
#define SCALAPACK_FLEUR -101
#endif

#ifdef __SCALAPACK
#define SCALAPACK_QE 103
#else
#define SCALAPACK_QE -103
#endif

#ifdef CPP_ELPA
#define ELPA_FLEUR 104
#else
#define ELPA_FLEUR -104
#endif

#ifdef CPP_ELPA_ONENODE
#define ELPASHARED_FLEUR 4
#else
#define ELPASHARED_FLEUR -4
#endif
